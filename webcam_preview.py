import numpy as np
import cv2
import time

def draw(frame, corners, framepts):
    corner = tuple(corners[0].ravel())
    frame = cv2.line(frame, corner, tuple(framepts[0].ravel()), (255,0,0), 5)
    frame = cv2.line(frame, corner, tuple(framepts[1].ravel()), (0,255,0), 5)
    frame = cv2.line(frame, corner, tuple(framepts[2].ravel()), (0,0,255), 5)
    return frame

def drawPos(frame, corners, framepts):
    frame = cv2.line(frame, tuple(framepts[0].ravel()), tuple(framepts[1].ravel()), (255,255,255), 5)
    frame = cv2.line(frame, tuple(framepts[1].ravel()), tuple(framepts[2].ravel()), (255,255,255), 5)
    frame = cv2.line(frame, tuple(framepts[2].ravel()), tuple(framepts[3].ravel()), (255,255,255), 5)
    frame = cv2.line(frame, tuple(framepts[3].ravel()), tuple(framepts[0].ravel()), (255,255,255), 5)
    return frame

def addTextGUI(frame, correctDistortion, cropROI, rescaleROI):
    frame = cv2.putText(frame, "ESC = Exit ", (10,10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (50, 50, 255))
    frame = cv2.putText(frame, "s = Save Frame ("+frameFilename+")", (10,H-30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (128, 255, 0))
    frame = cv2.putText(frame, "S = Save Calibration Frame", (10,H-15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (128, 255, 0))
    frame = cv2.putText(frame, "d = Correct Distortion " + ("(Yes)" if correctDistortion else "(No)"), (10,25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))

    if correctDistortion:
        frame = cv2.putText(frame, "c = Crop ROI " + ("(Yes)" if cropROI else "(No)"), (10,40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
        if cropROI:
            frame = cv2.putText(frame, "r = Rescale Crop " + ("(Yes)" if rescaleROI else "(No)"), (10,55), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
    return frame

XX = 3;
YY = 3 ;

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
objp = np.zeros((6*9,3), np.float32)
objp[:,:2] = np.mgrid[0:6,0:9].T.reshape(-1,2)
axis = np.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)

# Reads calibration values from the camera

cameraName = "logitech910"
inputPathFile = "output/"+cameraName+"_parameters.npz"
calibration = np.load(inputPathFile)

mtx = calibration["mtx"]
dist = calibration["dist"]
H = calibration["height"]
W = calibration["width"]
rvecs = calibration["rvecs"]
tvecs = calibration["tvecs"]
corners2 = calibration["corners2"]

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, W)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, H)
cap.set(cv2.CAP_PROP_FPS, 60)

newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(W,H),1,(W,H))

correctDistortion = False
cropROI = False
rescaleROI = False
showAxes = False

frameFilename = ""
sesionTimestamp = str(time.time())
sesionFrameID = 0

while(True):
    token = np.float32([[XX,YY,0], [XX+1,YY,0], [XX+1,YY+1,0], [XX,YY+1,0]]).reshape(-1,3)
    ret, frame = cap.read()
    originalFrame = frame.copy()

    if showAxes:
        imgpts, jac = cv2.projectPoints(axis, rvecs, tvecs, mtx, dist)
        frame = draw(frame,corners2,imgpts)

    imgpts, jac = cv2.projectPoints(token, rvecs, tvecs, mtx, dist)
    frame = drawPos(frame,corners2,imgpts)

    if correctDistortion:
        frame = cv2.undistort(frame, mtx, dist, None, newcameramtx)

        if cropROI:
            x,y,w,h = roi
            frame = frame[y:y+h, x:x+w]
            if rescaleROI:
                frame = cv2.resize(frame,(W,H), interpolation = cv2.INTER_CUBIC)


    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    elif k == ord(' '):
        showAxes = ~showAxes;
    elif k == ord('s'):
        frameFilename = cameraName+"_"+str(W)+"x"+str(H)+"_"+sesionTimestamp+"_"+format(sesionFrameID, "05")
        sesionFrameID += 1
        cv2.imwrite("output/"+frameFilename+".jpg", frame)
    elif k == ord('S'):
        cv2.imwrite("input/"+cameraName+".jpg", originalFrame)
    elif k == ord('d'):
        correctDistortion = ~correctDistortion
    elif k == ord('r'):
        rescaleROI = ~rescaleROI
    elif k == ord('c'):
        if correctDistortion:
            cropROI = ~cropROI
    elif k == ord('o'):
        XX = XX - 1 if XX > -1 else -1;
    elif k == ord('p'):
        XX = XX + 1 if XX < 5 else 5;
    elif k == ord('q'):
        YY = YY - 1 if YY > -1 else -1;
    elif k == ord('a'):
        YY = YY + 1 if YY < 8 else 8 ;


    frame = addTextGUI(frame, correctDistortion, cropROI, rescaleROI)

    cv2.imshow('frame',frame)

cap.release()
cv2.destroyAllWindows()
