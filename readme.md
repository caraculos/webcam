# Installation

* Install Python 2.7
* Download OpenCV somewhere in your dev environment
* Install Numpy 1.8
* Follow [this](https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_setup/py_setup_in_windows/py_setup_in_windows.html#install-opencv-python-in-windows) tutorial using Numpy 1.8 instead of 1.7:


## Configuration

* Print the pattern image in resources folder (chessboard pattern)
* Stick the printed pattern to a flat surface
* Point the webcam so the whole pattern can be seen
* Run webcam_preview.py and press S to generate a calibration image
* Press ESC to exit webcam_preview
* Run create_calibration_file.py
* Run webcam_preview.py again:
  * If you are interested in integrating 3D on the real image (extrinsic parameters are relevant), check if axes are drawn over the chessboard pattern
    * You can move the white framed square with qaop keys
    * These square borders should fit the calibration checkboard
  * If not, ignore the drawings and focus on the distortion.
    1. press d to correct **d**istortion. You will get a "spherized" image with a corrected image in the centre
    1. press c to **c**rop the valid area and ignore the over corrected borders
    1. press r to **r**escale the cropped image to the whole frame and original size (this is noisy, as any rescaling)
    1. press d repeatedly to see how the image switches from corrected to distorted back and forth
    1. press ESC to exit the preview

*Note:* Depending on the orientation of the chessboard the Z axis could not be well reconstructed. Keep this in mind when testing.

Units for physical coordinates (such as translation vector) are "square side" normalized. Z coordinate of this vector should be roughly the height of your camera above the chessboard surface.

## About calibration

* The process is splitted to state that calibrating and using are two clear independent steps
* Once we use many cameras, we will start having a "library" of intrinsic parameters
* Keep in mind that extrinsic parameters ("world references"), translation vector and rotation vector are only relevant in relation to the specific calibration scenario
* Intrinsic parameters are reusable if the camera moves but extrinsic are not (meaning that the reference system is attached to the camera itself)
